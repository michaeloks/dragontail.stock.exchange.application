package com.dragontail.Stock.Exchange.Application.Sources;

import com.dragontail.Stock.Exchange.Application.StockData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DBSource implements DataSource {
    private final StockDataRepository stockDataRepository;
    private final Logger logger = LoggerFactory.getLogger(DBSource.class);
    private void addDemoData(){
        StockData st = new StockData("AAPL" , 100 , 9616351663198L);
        this.stockDataRepository.save(st);

    }

    public DBSource(StockDataRepository stockDataRepository){
        this.stockDataRepository = stockDataRepository;
        addDemoData();
    }
    public List<StockData> findAllByNameIn(List<String> names){
        return stockDataRepository.findAllByNameIn(names);
    }

    @Override
    public List<StockData> getAll() {
        return stockDataRepository.findAll();
    }
}
