package com.dragontail.Stock.Exchange.Application;

import com.dragontail.Stock.Exchange.Application.Sources.DBSource;
import com.dragontail.Stock.Exchange.Application.Sources.DataSource;
import com.dragontail.Stock.Exchange.Application.Sources.LiveStockDataFetcher;
import com.dragontail.Stock.Exchange.Application.Sources.StockJsonFileReader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/stocks")
public class StockController {

    private final List<DataSource> dataSources = new LinkedList<>();

    public StockController(
            DBSource dbSource,
            StockJsonFileReader stockJsonFileReader,
            LiveStockDataFetcher liveStockDataFetcher
    ) {
        dataSources.add(stockJsonFileReader);
        dataSources.add(dbSource);
        dataSources.add(liveStockDataFetcher);
    }


    @GetMapping
    public List<StockData> getAllStocks() {
        return getStocksByNames(null);
    }

    @GetMapping(params = "names")
    public List<StockData> getStocksByNames(@RequestParam("names") List<String> names) {
        HashMap<String, StockData> stockDataMap = new HashMap<>();

        for (var source : dataSources) {
            List<StockData> stockDataFromSource;
            if (names == null) {
                stockDataFromSource = source.getAll();
            } else {
                stockDataFromSource = source.findAllByNameIn(names);
            }

            if (stockDataFromSource == null) {
                continue;
            }
            for (var sd : stockDataFromSource) {
                if (!stockDataMap.containsKey(sd.getName())) {
                    stockDataMap.put(sd.getName(), sd);
                    continue;
                }

                if (stockDataMap.get(sd.getName()).getDate() < sd.getDate()) {
                    stockDataMap.put(sd.getName(), sd);
                }
            }
        }
        return stockDataMap.values().stream().toList();
    }

}
