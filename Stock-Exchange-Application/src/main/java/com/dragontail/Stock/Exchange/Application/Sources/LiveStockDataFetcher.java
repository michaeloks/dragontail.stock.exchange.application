package com.dragontail.Stock.Exchange.Application.Sources;

import com.dragontail.Stock.Exchange.Application.Sources.DataSource;
import com.dragontail.Stock.Exchange.Application.StockData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Component
public class LiveStockDataFetcher implements DataSource {
    private static final String S3_ENDPOINT_URL = "https://test-stocks-data.s3.us-east-2.amazonaws.com/stocksData.json";
    List<StockData> cacheData;
    private long cacheTime = 0;
    private final int cacheDurationInSeconds = 10;
    private final Logger logger = LoggerFactory.getLogger(LiveStockDataFetcher.class);
    public void updateStockData() {
        if(((System.currentTimeMillis() - cacheTime)/1000) < cacheDurationInSeconds) {
            logger.info("LiveStockDataFetcher - using cache");
            return;
        }

        cacheTime = System.currentTimeMillis();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(S3_ENDPOINT_URL).build();

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful() && response.body() != null) {
                String jsonData = response.body().string();

                ObjectMapper objectMapper = new ObjectMapper();
                TypeReference<List<StockData>> typeReference = new TypeReference<>() {};
                cacheData = objectMapper.readValue(jsonData, typeReference);

                logger.info("LiveStockDataFetcher - Downloaded " + cacheData.size() + " stockes ");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("LiveStockDataFetcher - Loaded empty file");
        cacheData = Collections.emptyList();
    }

    @Override
    public List<StockData> findAllByNameIn(List<String> names) {
        updateStockData();
        List<StockData> filterd = new LinkedList<>();

        for(var name : names) {
            for (var st : cacheData) {
                if (st.getName().equals(name)){
                    filterd.add(st);
                }
            }
        }
        return filterd;
    }

    @Override
    public List<StockData> getAll() {
        updateStockData();
        return cacheData;
    }
}
