package com.dragontail.Stock.Exchange.Application.Sources;

import com.dragontail.Stock.Exchange.Application.StockData;

import java.util.List;

public interface DataSource {

    List<StockData> findAllByNameIn(List<String> names);

    List<StockData> getAll();


}
