package com.dragontail.Stock.Exchange.Application.Sources;

import com.dragontail.Stock.Exchange.Application.StockData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Component
public class StockJsonFileReader implements DataSource {
    private static final String JSON_FILE_PATH = "stocks.json";
    private final Logger logger = LoggerFactory.getLogger(StockJsonFileReader.class);
    HashMap<String , StockData> cache;
    FileTime fileTime;

    private void loadStockData() {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String path = Objects.requireNonNull(getClass().getClassLoader().getResource(JSON_FILE_PATH)).getPath();
            BasicFileAttributes attr = Files.readAttributes(Paths.get(path), BasicFileAttributes.class);

            if(fileTime != null && fileTime.equals(attr.lastModifiedTime())){
                logger.info("loading from cache");
                return;
            }
            fileTime =  attr.lastModifiedTime();

            InputStream inputStream = new FileInputStream(path);
            TypeReference<List<StockData>> typeReference = new TypeReference<>() {};
            List<StockData> stockData = objectMapper.readValue(inputStream, typeReference);
            HashMap<String , StockData> stocks = new HashMap<>();
            for(var st : stockData){
                stocks.put(st.getName(), st);
            }
           this.cache = stocks;
            logger.info("loading from file " + cache.size() + " stockes");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public StockJsonFileReader(){
        loadStockData();
    }

    @Override
    public List<StockData> findAllByNameIn(List<String> names) {
        loadStockData();
        List<StockData> filtered = new LinkedList<>();
        for(var name : names){
            filtered.add(cache.get(name));
        }
        return filtered;
    }

    @Override
    public List<StockData> getAll() {
        return cache.values().stream().toList();
    }
}

