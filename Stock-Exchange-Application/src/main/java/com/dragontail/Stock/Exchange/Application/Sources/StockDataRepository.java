package com.dragontail.Stock.Exchange.Application.Sources;

import com.dragontail.Stock.Exchange.Application.StockData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockDataRepository extends JpaRepository<StockData, Long> {
        List<StockData> findAllByNameIn(List<String> names);
}
